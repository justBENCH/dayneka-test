'use strict';

// Modal
const btnOpenModal = document.querySelector('#call_link');
const btnCloseModal = document.querySelector('#close_modal');
const modalElement = document.querySelector('#modal');

const toggleModal = () => {
	modalElement.classList.toggle('show');
};

btnOpenModal.addEventListener('click', toggleModal);
btnCloseModal.addEventListener('click', toggleModal);

// Benefits

const btnOpenBenefits = document.querySelector('#open_benefits');
const benefitsElement = document.querySelector('#benefits');

const toggleBenefits = () => {
	benefitsElement.classList.toggle('show');

	const img = btnOpenBenefits.children[0];
	const isOpen = benefitsElement.classList.contains('show');

	if (isOpen) {
		img.src = './img/icons/close.svg';
	} else {
		img.src = './img/icons/plus.svg';
	}
};

btnOpenBenefits.addEventListener('click', toggleBenefits);

// Menu

const btnMenu = document.querySelector('#menu');
const mainContainerElement = document.querySelector('.container__main');

const BG_STYLE = mainContainerElement.style;

const toggleMenu = () => {
	mainContainerElement.classList.toggle('show_menu');

	const img = btnMenu.children[0];
	const isOpen = mainContainerElement.classList.contains('show_menu');

	if (isOpen) {
		img.src = './img/icons/menu_close.svg';
		mainContainerElement.style.background = '#EDF2F5';
	} else {
		img.src = './img/icons/menu_open.svg';
		mainContainerElement.style = BG_STYLE;
	}
};

btnMenu.addEventListener('click', toggleMenu);

// Phone formatter

const inputElement = document.querySelector('#phone_number');
const policyCheckBox = document.querySelector('#policy');
const btnSubmitPhone = document.querySelector('#submit_phone');

const buttonDisabledToggle = () => {
	btnSubmitPhone.disabled = !policyCheckBox.checked || inputElement.value.length < 16;
}

const isNumericInput = (event) => {
	const key = event.keyCode;
	return ((key >= 48 && key <= 57) || // Allow number line
		(key >= 96 && key <= 105) // Allow number pad
	);
};

const isModifierKey = (event) => {
	const key = event.keyCode;
	return (event.shiftKey === true || key === 35 || key === 36) ||
		(key === 8 || key === 9 || key === 13 || key === 46) ||
		(key > 36 && key < 41) ||
		(
			(event.ctrlKey === true || event.metaKey === true) &&
			(key === 65 || key === 67 || key === 86 || key === 88 || key === 90)
		);
};

const enforceFormat = (event) => {
	if (!isNumericInput(event) && !isModifierKey(event)) {
		event.preventDefault();
	}
};

const formatToPhone = (event) => {
	buttonDisabledToggle();

	if (isModifierKey(event)) {
		return;
	}

	const input = event.target.value.replace(/\D/g, '').substring(0, 10);

	const code = input.substring(0, 1);
	const areaCode = input.substring(1, 4);
	const first = input.substring(4, 6);
	const second = input.substring(6, 8);
	const last = input.substring(8, 10);

	if (input.length > 8) {
		event.target.value = `+${code} (${areaCode})${first}-${second}-${last}`;
	} else if (input.length > 6) {
		event.target.value = `+${code} (${areaCode})${first}-${second}`;
	} else if (input.length > 4) {
		event.target.value = `+${code} (${areaCode})${first}`;
	} else if (input.length > 1) {
		event.target.value = `+${code} (${areaCode}`;
	} else if (input.length > 0) {
		event.target.value = `+${code}`;
	}
};



inputElement.addEventListener('keydown', enforceFormat);
inputElement.addEventListener('keyup', formatToPhone);

policyCheckBox.addEventListener('change', buttonDisabledToggle);
btnSubmitPhone.addEventListener('click', (e) => e.preventDefault());







